{
  name = "nix";
  toolbar = true;
  bookmarks = [
    {
      name = "nix_";
      bookmarks = [
        {
          name = "";
          url = "https://github.com/nix-community/nix-on-droid";
        }
      ];
    }
    {
      name = "nix";
      bookmarks = [
        {
          name = "nixd";
          url = "https://github.com/nix-community/nixd";
        }
        {
          name = "nix.dev/";
          url = "https://nix.dev/";
        }
        {
          name = "nixos.org/manual/nixos/";
          url = "https://nixos.org/manual/nixos/unstable/";
        }
        {
          name = "nixos.org/manual/nixpkgs/";
          url = "https://nixos.org/manual/nixpkgs/unstable/";
        }
        {
          name = "wiki.nixos.org/wiki";
          url = "https://wiki.nixos.org/wiki/NixOS_Wiki";
        }
        {
          name = "github:nix-community/NUR";
          url = "https://github.com/nix-community/NUR";
        }
        {
          name = "nur.nix-community.org/repos/rycee/";
          url = "https://nur.nix-community.org/repos/rycee/";
        }
        {
          name = "noogle.dev/";
          url = "https://noogle.dev/";
        }
      ];
    }
  ];
}
