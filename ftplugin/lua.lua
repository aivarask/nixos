-- vim.notify('after/ftplugin/lua.lua')
vim.opt.listchars = {
	space = '·',
	tab = '  ┊',
	-- eol = '↵'
}
vim.opt.list = true
