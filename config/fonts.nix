{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [ fontpreview ];
  fonts = {
    enableDefaultPackages = false;
    packages = with pkgs; [
      noto-fonts-color-emoji
      nerd-fonts.dejavu-sans-mono
    ];
    fontconfig = {
      defaultFonts = {
        emoji = [
          "Noto Color Emoji"
          "DejaVuSansM Nerd Font"
        ];
        monospace = [
          "DejaVuSansM Nerd Font Mono"
          "Noto Color Emoji"
        ];
        sansSerif = [
          "DejaVu Sans"
          "Noto Color Emoji"
        ];
        serif = [
          "DejaVu Sans"
          "Noto Color Emoji"
        ];
      };
    };
  };
}
