{
  outputs = _: {
    nixosModules.default =
      { pkgs, ... }:
      {
        environment.systemPackages = with pkgs; [
          git
          git-lfs
          gh
          git-crypt
          pre-commit
        ];
      };
    nixosModules.home = _: {
      programs.git = {
        delta.enable = false;
        lfs.enable = true;
        enable = true;
        aliases = {
          ci = "commit";
          pr = "pull --rebase";
        };
        extraConfig = {
          core.hookspath = ".githooks";
          init.defaultBranch = "main";
          pull.rebase = true;
        };
        ignores = [
          ".direnv/"
          "*.lock"
          "!flake.lock"
          "*lock.json"
          "*lock.yaml"
          "tags"
          "node_modules/"
          "vendor/"
          "result/"
          "result/null"
          "CMakeFiles/"
          "Session.vim"
          ".pytest_cache/"
          "__pycache__"
          ".zig-cache/"
        ];
        userName = "Aivaras Kalesnykas";
        userEmail = "kalesnykas.aivaras@gmail.com";
      };
    };
  };
}
