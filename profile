# vim: ft=sh
# sed -i -E 's/^(ENABLED\s*=\s*)1$/\10/; t; s/^(ENABLED\s*=\s*)0$/\11/' profile
# ENABLED=1
export FZF_DEFAULT_OPTS="${FZF_DEFAULT_OPTS} --info=inline"
export __ETC_PROFILE_SOURCED=
export A=7
export __ETC_PROFILE_DONE=
export WEZTERM_CONFIG_FILE=/etc/nixos/programs/wezterm.lua
