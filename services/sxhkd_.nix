{ ... }:
{
  home.sessionVariables.SXHKDRC = "/etc/nixos/services/sxhkdrc";
  services.sxhkd.enable = true;
}
