# vim: ft=bash

# /etc/bashrc

# export __ETC_BASHRC_SOURCED=
# export __NIXOS_SET_ENVIRONMENT_DONE=
# source /etc/nixos/profile

# bind '"\e[24~":"pwd\n"'
# bind '"^[[15~" "exec zsh\n"' # <F5>
